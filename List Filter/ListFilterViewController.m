//
//  ListFilterViewController.m
//  List Filter
//
//  Created by Ariel Fertman on 2/16/2014.
//  Copyright (c) 2014 Ariel Fertman. All rights reserved.
//

#import "ListFilterViewController.h"

@interface ListFilterViewController ()

@property (nonatomic, readwrite, weak)IBOutlet UITextField *tvFilter;
@property (strong, nonatomic)IBOutlet UITableView *tableView;
@property (nonatomic, readwrite, weak)IBOutlet UILabel *matchLbl;
@property (strong, nonatomic)ListModel *listModel;
@property (strong, nonatomic)NSMutableArray *filterdList;

- (IBAction)filterChanged:(UITextField *)sender;

@end

@implementation ListFilterViewController

@synthesize tvFilter = _tvFilter;
@synthesize tableView = _tableView;
@synthesize listModel = _listModel;
@synthesize filterdList = _filterdList;
@synthesize matchLbl = _matchLbl;


- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"View did load, init of data");
    _listModel = [[ListModel alloc] init];
    _filterdList = _listModel.tableData; //init to data source at first
    _matchLbl.text = [NSString stringWithFormat:@"Matches: %lu", _filterdList.count];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)filterChanged:(UITextField *)sender
{
    _filterdList = [_listModel sortWithSquence:sender.text];
    _matchLbl.text = [NSString stringWithFormat:@"Matches: %lu", _filterdList.count];
    [_tableView reloadData];
}

//table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _filterdList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //here we create a simple identifier for reusability
    static NSString *cellID = @"SimpleID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) { //check if its nil, if it is we need to create a simple cell with the default style
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    NSString *cellText = [_filterdList objectAtIndex:indexPath.row];
    cell.textLabel.text = cellText; //set the inner cell label text
    
    return cell;
}

@end
