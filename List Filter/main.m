//
//  main.m
//  List Filter
//
//  Created by Ariel Fertman on 2/16/2014.
//  Copyright (c) 2014 Ariel Fertman. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ListFilterAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ListFilterAppDelegate class]));
    }
}
