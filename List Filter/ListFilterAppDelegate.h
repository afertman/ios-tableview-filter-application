//
//  ListFilterAppDelegate.h
//  List Filter
//
//  Created by Ariel Fertman on 2/16/2014.
//  Copyright (c) 2014 Ariel Fertman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListFilterAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
