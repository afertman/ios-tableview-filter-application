//
//  ListModel.m
//  List Filter
//
//  Created by Ariel Fertman on 2/16/2014.
//  Copyright (c) 2014 Ariel Fertman. All rights reserved.
//

#import "ListModel.h"

@implementation ListModel

@synthesize tableData = _tableData;
@synthesize filteredList = _filteredList;

- (id) init
{
    //init balance
    if(self == [super init])
    {
        NSLog(@"Creating table data");
        
        // Path to the plist (in the application bundle)
        NSString *path = [[NSBundle mainBundle] pathForResource:
                          @"words" ofType:@"plist"];
        
        // Build the array from the plist
        _tableData  = [[NSMutableArray alloc] initWithContentsOfFile:path];
        
//        _tableData = [NSMutableArray arrayWithObjects:@"Egg Benedict", @"Mushroom Risotto", @"Full Breakfast", @"Hamburger", @"Ham and Egg Sandwich", @"Creme Brelee", @"White Chocolate Donut", @"Starbucks Coffee", @"Vegetable Curry", @"Instant Noodle with Egg", @"Noodle with BBQ Pork", @"Japanese Noodle with Pork", @"Green Tea", @"Thai Shrimp Cake", @"Angry Birds Cake", @"Ham and Bacon Panini", @"Ham and Cheese Panini",@"Turkey", nil];
        
        _filteredList = [[NSMutableArray alloc] init];
        NSLog(@"Created %lu rows", _tableData.count);
    }
    return self;
}


- (NSMutableArray *)sortWithSquence:(NSString *)sequence
{
    if([sequence isEqualToString:@""]) //if no filter, return the regular list
    {
        return _tableData;
    }
    
    //remake the filtered list
    [_filteredList removeAllObjects];
    
    //O(n)
    for(NSString *str in _tableData) //loop through entire set each time for now ..
    {
        NSRange textRange;
        textRange =[str rangeOfString:sequence options:NSCaseInsensitiveSearch];
        
        if(textRange.location != NSNotFound)
        {
            [_filteredList addObject:str];
        }
    }
    return _filteredList;
}

@end
