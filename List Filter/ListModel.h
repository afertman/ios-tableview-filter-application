//
//  ListModel.h
//  List Filter
//
//  Created by Ariel Fertman on 2/16/2014.
//  Copyright (c) 2014 Ariel Fertman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListModel : NSObject

@property (nonatomic, readwrite, strong) NSMutableArray *tableData;
@property (nonatomic, readwrite, strong) NSMutableArray *filteredList;

- (NSMutableArray *)sortWithSquence:(NSString *)sequence;

@end
